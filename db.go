package crawler

import (
	"database/sql"
	"log"

	"github.com/eaigner/hood"
)

// Datos de conexión a la base de datos
// - Documentación campos: https://godoc.org/github.com/lib/pq
const datosConexionPostgreSQL = "user=root host=127.0.0.1 dbname=tfg sslmode=require"

func ConectarConBaseDeDatos() *hood.Hood {
	return hood.New(
		ConectarConBaseDeDatosRaw(),
		hood.NewPostgres(),
	)
}

func ConectarConBaseDeDatosRaw() *sql.DB {
	db, err := sql.Open("postgres", datosConexionPostgreSQL)
	if err != nil {
		log.Fatal(err)
	}

	return db
}
