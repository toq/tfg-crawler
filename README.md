# tfg-crawler

Este repositorio contiene el código del crawler utilizado en el Trabajo de Fin de Grado de N. M. M. del Grado de Información y Documentación.

## Requisitos

- [Go](http://golang.org) 1.x, idealmente 1.6.x
- [PostgreSQL](http://postgresql.org) 9.5

## Preparativos

1. Instalar dependencias: `go get ./...`
2. Crear archivo ejecutable: `go build -o crawler.exe cmd/crawler/crawler.go`
3. Crear tablas: `crawler.exe -crear-tablas`

## Ejecución

1. Editar el archivo [db.go] con los datos adecuados de conexión a PostgreSQL.
2. `crawler.exe -ruta=bibliotecas.csv -descargas=8`
