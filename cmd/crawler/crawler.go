package main

// Librerías que usa el programa
import (
	// Primer bloque: librerías del lenguaje
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	// Segundo bloque: librerías definidas por nosotros para este programa
	"gitlab.com/toq/tfg-crawler"
	"gitlab.com/toq/tfg-crawler/modelos"

	// Tercer bloque: librerías de terceros
	"github.com/PuerkitoBio/goquery"
	"github.com/eaigner/hood"
	"github.com/fatih/color"
	"github.com/gosuri/uiprogress"
	"golang.org/x/text/encoding/charmap"
)

// Esto es un bloque de variables GLOBALES
//
// Todos los bloques 'var' y 'const' que hay fuera de una función son globales,
// es decir, que se pueden acceder desde cualquier sitio.
var (
	// Esto es el cliente HTTP (software) que vamos a usar
	// para realizar *TODAS* las peticiones del programa
	client = &http.Client{
		Timeout: 15 * time.Second,
	}

	// Opciones o 'flags' que podemos pasarle al programa por línea de comandos
	flagCrearTablas             bool   // verdadero/falso
	flagBorrarTablas            bool   // verdadero/falso
	flagNumDescargasSimultaneas int    // número con signo sin decimales
	flagRutaArchivoDatos        string // texto
)

// La función 'main' es la función principal del programa. Es OBLIGATORIO
// definirla porque es desde donde el programa arranca y comienza a hacer su
// trabajo.
func main() {
	// Establecer zona horaria, por si acaso
	time.LoadLocation("Europe/Madrid")

	// Definición de las opciones del programa
	// Todas las funciones del estilo 'flag.LOQUESEAVar' tienen los mismos argumentos:
	// (1) una referencia a una variable (por eso ponemos el '&') para guardar un valor
	// (2) el nombre de la opción
	// (3) un valor por defecto
	// (4) una descripción
	flag.BoolVar(&flagCrearTablas, "crear-tablas", false, "crea tablas e índices en la base de datos")
	flag.BoolVar(&flagBorrarTablas, "borrar-tablas", false, "borra tablas e índices en la base de datos")
	flag.IntVar(&flagNumDescargasSimultaneas, "descargas", 4, "número de descargas simultaneas permitidas")
	flag.StringVar(&flagRutaArchivoDatos, "ruta", "", "ruta a un archivo '.csv' que contiene las columnas: 'nombre,codigo,lugar,codigo_postal'")

	// Después de definir como se llaman las opciones y donde vamos a guardar
	// sus valores solo queda que el programa lea lo que le llega y rellena las
	// opciones adecuadamente
	flag.Parse()

	// A partir de aquí ya podemos empezar a hacer cosas del crawler

	// Como las opciones de crear y borrar tablas son valores de verdadero/falso
	// vamos a comprobar si su valor es verdadero, y de ser así ejecutamos la
	// acción correspondiente a cada una de ellas.
	//
	// En este caso como son acciones bastante importantes y previas al
	// funcionamiento normal del crawler cuando alguna de las dos opciones se
	// utiliza el programa termina.
	//
	// Como los 'if' están en un cierto orden, si llamamos a: 'crawler.exe
	// -crear-tablas -borrar-tablas' pese a que las dos opciones están puestas,
	// solo vamos a hacer la primera porque hemos configurado el programa para
	// que con cualquiera de estas dos opciones pare inmediatamente. Luego para
	// borrar y luego crear las tablas, lo que sería un reset de la base de
	// datos, tenemos que hacer dos llamadas al programa.
	if flagCrearTablas {
		CrearTablasBaseDeDatos()
		return
	}
	if flagBorrarTablas {
		BorrarTablasBaseDeDatos()
		return
	}

	// Aquí hacemos una comprobación, el programa necesita la ruta de archivo a
	// los datos para funcionar así que si el texto tiene longitud 0 significa
	// que no nos han pasado lo que necesitábamos así que vamos a salir del
	// programa mostrando un error.
	if len(flagRutaArchivoDatos) == 0 {
		fmt.Println(
			// Usamos la librería 'color' para mostrar texto de color en la consola
			color.RedString("error!"),
			"El crawler necesita la '-ruta' al archivo de datos",
		)

		return
	}

	// Por fin, si llegamos a este punto significa que todos los requisitos se
	// cumplen (las opciones que hagan falta se han rellenado adecuadamente, o
	// al menos tienen un valor y vamos a empezar a crawlear la página del OPAC
	// de Madrid.
	Crawlear(flagRutaArchivoDatos)
	// LoadLibraries(flagRutaArchivoDatos)
}

// Esta es una función de utilidad ('helper' en inglés) que recibe un error
// cualquier y si este no es nulo ('nil') entonces lo imprime por pantalla y
// fuerza a que el programa termine esté donde esté.
func AbortarSi(err error) {
	if err != nil {
		log.Fatalln("fatal:", err)
	}
}

// La función 'padre' por así llamarla que gestiona todo el 'sarao' del crawler.
// Necesita la ruta al archivo de datos para funcionar.
func Crawlear(input string) {
	// Primero nos guardamos la fecha y hora actual, esto nos servirá para
	// mostrar cuánto tiempo ha tardado el crawler en hacer su trabajo.
	inicioFuncion := time.Now()

	// Luego mostramos unos mensajes por pantalla con los valores de las
	// opciones que hemos recibido, esto viene bien para ver con qué paramétros
	// hemos ejecutado el crawler esta vez y además para ver que estamos leyendo
	// los valores de las opciones bien.
	fmt.Println("Comienzo a trabajar...")
	fmt.Println("Opciones:")
	fmt.Printf("- %s descargas simultáneas\n", color.YellowString("%d", flagNumDescargasSimultaneas))
	fmt.Println()

	// Vamos a abrir una nueva conexión con la base de datos, la necesitamos
	// para poder guardar datos en las tres tablas que tenemos.
	conexionBaseDatos := crawler.ConectarConBaseDeDatos()

	// Intentamos leer el archivo de datos, como es un '.csv' vamos a obtener
	// una lista de listas. O lo que es lo mismo una lista de con todas las
	// filas del archivo, las filas se construyen separando por las comas de
	// cada línea del archivo. Ejemplo:
	//
	//      Nombre,Codigo,Lugar,CodigoPostal
	//      Biblioteca Nacional,BN000,Madrid,28001
	//      Biblioteca Regional,BR000,Madrid,28002
	//      ...
	filas, err := LeerArchivoDatos(input)
	AbortarSi(err)

	// Aquí vamos a recorrer la lista de filas saltándonos la primera que tiene
	// los encabezados (el nombre de cada columna) y con cada fila vamos a
	// acceder al formulario de Nuevas Adquisiciones, usar el código de
	// biblioteca para extraer la 'URL patrón' y el número total de filas de
	// cada biblioteca. Una vez tengamos eso podemos empezar a descargar
	// información de libros.
	for _, fila := range filas[1:] {
		DescargarLibrosBiblioteca(conexionBaseDatos, fila)
	}

	// Por último, vamos a imprimir por consola que el crawler ha terminado y
	// cuánto tiempo le ha costado descargar toda la información.
	finFuncion := time.Since(inicioFuncion)
	fmt.Println("Terminado")
	fmt.Println("He tardado", color.YellowString(finFuncion.String()))
}

// Lee un archivo '.csv' que está en 'ruta' y devuelve la lista de filas.
func LeerArchivoDatos(ruta string) (filas [][]string, err error) {
	lectorArchivo, err := os.Open(ruta)
	if err != nil {
		return nil, err
	}

	defer func() {
		err = lectorArchivo.Close()
	}()

	lectorCSV := csv.NewReader(lectorArchivo)
	lectorCSV.TrimLeadingSpace = true

	return lectorCSV.ReadAll()
}

// A partir del código de una biblioteca vamos a sacar la 'URL patrón' y el
// número total de libros. Luego esa información la pasaremos a una o más
// funciones encargas de descargar los libros. Cada una de estas funciones
// trabaja en su propio hilo (queremos descargar varias cosas a la vez!) así
// podemos sacar el máximo rendimiento.
func DescargarLibrosBiblioteca(conexionBaseDatos *hood.Hood, fila []string) {
	inicioFuncion := time.Now()

	// 'fila' es una lista de valores de tipo texto, concretamente y para este
	// 'caso son solo 4 valores y este es su orden:
	//
	// - Nombre biblioteca
	// - Codigo biblioteca
	// - Lugar
	// - Codigo Postal
	GuardarBibliotecaEnBaseDeDatos(conexionBaseDatos, fila)

	// En esta función solo necesitamos los dos primeros valores así que nos
	// olvidamos de los otros.
	nombreBiblioteca := fila[0]
	codigoBiblioteca := fila[1]
	// lugar := fila[2]
	// codigoPostal := fila[3]

	// Para poder seguir el progreso del crawler vamos a imprimir el nombre de
	// la biblioteca.
	fmt.Println(nombreBiblioteca)

	// Comenzamos la descarga de libros, que mostrará otros mensajes de progreso
	// a su vez...
	DescargarLibrosCodigoBiblioteca(conexionBaseDatos, codigoBiblioteca)

	// Y una vez acabemos vamos a mostrar por consola cuanto tiempo se ha tardado
	finFuncion := time.Since(inicioFuncion)
	fmt.Println("He tardado", color.YellowString(finFuncion.String()))
}

// Según vayamos leyendo filas del archivo '.csv' iremos guardando en la base de
// datos información de cada biblioteca para poder luego asociar libros a
// bibliotecas a través del código de estas. Esta función se encarga de guardar
// la información de las bibliotecas en la base de datos.
func GuardarBibliotecaEnBaseDeDatos(conexionBaseDatos *hood.Hood, fila []string) {
	// Esta es la representación en código de lo que vamos a guardar en la base
	// de datos, concretamente en la tabla de Bibliotecas.
	biblioteca := &modelos.Biblioteca{
		Nombre:       fila[0],
		Codigo:       fila[1],
		Lugar:        fila[2],
		CodigoPostal: fila[3],
	}

	// Vamos a imprimir por pantalla un mensaje y según haya errores o no
	// mostraremos un 'ok' en verde o un 'error' en rojo con la información del
	// error, por si acaso.
	fmt.Printf("Guardando datos de %s ", color.YellowString(biblioteca.Nombre))

	// Vamos a guardar y a ver que pasa...
	_, err := conexionBaseDatos.Save(biblioteca)
	if err != nil {
		// ... si hay algún error lo mostramos...
		fmt.Println(color.RedString("error"), biblioteca.Codigo, err)
		// ... y la función acaba!
		return
	}

	// ... si llegamos aquí es que no hay error, así que un 'ok' en verde y listo!
	fmt.Println(color.GreenString("ok"))
}

func DescargarLibrosCodigoBiblioteca(conexionBaseDatos *hood.Hood, codigoBiblioteca string) {
	// (0) Cada vez que llamemos a esta función vamos a poner a funcionar una
	// cantidad de funciones en hilos independientes que se encargarán de
	// descargar libros.

	// (1) Necesitamos una sesión para trabajar con la página del OPAC
	sesion := NuevaSesion()

	// (2) Con esa sesion, rellenamos y enviamos el formulario de busqueda para
	// una biblioteca en concreto usando su código.
	urlFormulario, pagina := EnviarFormularioNuevasAdquisiciones(sesion, codigoBiblioteca)

	// (3) Con la página que nos devuelvan podemos sacar la 'URL patrón' y el
	// número total de libros.
	urlPatron := ExtraerURLPatron(pagina)
	total := ExtraerTotalLibros(pagina)

	// (3.1) Como la URL que sacamos es parcial tenemos que añadirle la del
	// formulario para tener una URL válida, con su 'http://' el dominio
	// madrid.org y la ruta bien puesta.
	urlPatron = urlFormulario + urlPatron
	// (3.2) El final contiene '&DOC=1', nosotros queremos solo '&DOC=' sin el '1'.
	urlPatron = strings.TrimSuffix(urlPatron, "1")

	// (4) Barras de progreso para poder seguir el progreso del crawler.
	// No es importante saber que hace esta parte de código.
	uiprogress.Start()
	barraProgreso := uiprogress.AddBar(total)
	barraProgreso.AppendFunc(func(b *uiprogress.Bar) string {
		return fmt.Sprintf("%3d/%3d %s", b.Current(), b.Total, b.CompletedPercentString())
	})
	barraProgreso.PrependElapsed()

	var (
		// Un WaitGroup es un mecanismo de sincronización para gestionar 2 o más
		// funciones que trabajan en hilos independientes. Básicamente nos sirve
		// para controlar cuántas hay al mismo tiempo trabajando y notificar al
		// programa de cuándo han acabado su trabajo.
		wg sync.WaitGroup
		// Estas dos variables son unos contadores, uint32 señaliza que son
		// enteros (int) sin signo (u) de 32 bits, luego almacen un número entre
		// 0 y 4294967296, extremos incluídos.
		numeroBien = uint32(0)
		numeroMal  = uint32(0)
	)

	// Creamos un 'canal' de tipo 'string' o lo que es lo mismo una cola en la
	// que podemos introducir variables de tipo texto ('string'). Esto lo vamos
	// a usar para enviar URLs a las funciones que trabajan en su propio hilo
	// para descargar la información de cada libro.
	var tareas = make(chan string)

	// A continuación lanzamos varias funciones de descarga en hilos diferentes
	// (palabra clave 'go'). Esto es una parte importante del programa pero
	// tampoco hace falta entrar en demasiado detalle.
	//
	// Básicamente limitamos el número total de funciones que descargan libros
	// para no saturar nuestra máquina ni el servidor remoto al que queremos
	// crawlear.
	for i := 0; i < flagNumDescargasSimultaneas; i++ {
		// Indicamos al mecanismo de sincronización que se va a arrancar una
		// función en otro hilo.
		wg.Add(1)

		// Aquí declaramos una función anónima dentro de la que ya estamos
		// (funciones dentro de funciones!) y mediante la palabra clave 'go'
		// hacemos que se ejecute en otro hilo.
		//
		// Como parámetro le pasamos la cola por la que irá leyendo URLs de
		// donde descargar libros.
		go func(tareas chan string) {
			// Recorremos la cola y según obtengamos una URL de la que descargar
			// un libro comenzamos a descargarlo. Cuando no queden más tareas
			// saldremos del for, nunca antes.
			for urlLibro := range tareas {
				// Por cada URL de libro nos bajamos su página y la guardamos en
				// la base de datos. O al menos lo intentamos, puede fallar algo
				// por el camino.
				DescargarYGuardarLibro(
					conexionBaseDatos,
					codigoBiblioteca,
					urlLibro,
					&numeroBien,
					&numeroMal,
					barraProgreso,
				)
			}

			// Nos hemos quedado sin tareas así avisamos al mecanismo de
			// sincronización que esta función ha acabado de descargar (puede
			// haber otras todavía trabajando!).
			wg.Done()
		}(tareas)
	}

	// Una vez tenemos las N funciones que van a descargar la información de los
	// libros preparadas, empezamos a enviarles trabajo para hacer. Qué
	// enviamos? Sabemos el total de libros y la 'URL-patrón' solo queda crear
	// tantas URL de libros como libros haya en total añadiendo el número de
	// libro al final de la URL.
	//
	// Ejemplo:
	// - URL patrón: "http://www.madrid.org/opac?libro="
	// - Total libros: 200
	//
	// Con un bucle creamos las 200 URLs del estilo
	// "http://www.madrid.org/opac?libro=1",
	// "http://www.madrid.org/opac?libro=2",
	// "http://www.madrid.org/opac?libro=3",
	// .................................,
	// "http://www.madrid.org/opac?libro=200"
	for n := 1; n <= total; n++ {
		// Une 'urlPatron' con el número de libro 'n', de paso ya que está
		// convierte 'n' que es un número en texto.
		//
		// Ejemplo:
		//   urlPatron = "http://www.madrid.org/opac?libro="
		//           n = 42
		//   -------------------------------------------------
		//    urlLibro = "http://www.madrid.org/opac?libro=42"
		urlLibro := fmt.Sprintf("%s%d", urlPatron, n)

		// Esta línea mete 'urlLibro' en la cola de 'tareas', ni más ni menos.
		tareas <- urlLibro
	}

	// Con 'close' cerramos una cola. No tenemos trabajo infinito para nuestras
	// funciones de descarga!
	close(tareas)

	// Esperamos a que las funciones terminen de trabajar...
	wg.Wait()

	// Y mostramos el resultado final para esta biblioteca
	fmt.Println("Biblioteca crawleada!")
	fmt.Printf("Bien: %3d/%3d\n", numeroBien, total)
	fmt.Printf(" Mal: %3d/%3d\n", numeroMal, total)
}

// Para poder interactuar con la página del OPAC necesitamos poder obtener una
// sesión nueva siempre que queramos para que en caso de caducar la que estemos
// usando podamos seguir trabajando con una nueva como si no hubiera pasado
// nada.
//
// Las sesiones no son ni más ni menos que un identificador que el servidor de
// madrid.org usa para identificar todos y cada uno de sus usuarios, en nuestro
// caso un cliente HTTP vía software.
func NuevaSesion() string {
	const (
		BaseUrl    = "http://www.madrid.org/biblio_publicas/cgi-bin/abnetopac"
		MetaPrefix = "0; URL="
		MetaSuffix = "?ACC=101"
		RootUrl    = "http://www.madrid.org"
	)

	// Descarga el código HTML de `BaseUrl` y lo prepara para buscar en él
	doc, err := DescargarPagina(BaseUrl)
	AbortarSi(err)

	// Buscamos las etiquetas <meta>
	content, _ := doc.Find("meta").
		// Nos quedamos la 4a:
		// <meta http-equiv="Refresh" content="0; URL=/biblio_publicas/cgi-bin/abnetopac/O9175/ID283f0afd?ACC=101" />
		Eq(3).
		// Cogemos el valor de su attributo 'content':
		// "0; URL=/biblio_publicas/cgi-bin/abnetopac/O9175/ID283f0afd?ACC=101"
		Attr("content")

	// Y limpiamos el valor de 'content' para quedarnos con lo que nos interesa
	// "/biblio_publicas/cgi-bin/abnetopac/O9175/ID283f0afd?ACC=101"
	content = strings.TrimPrefix(content, MetaPrefix)
	// "/biblio_publicas/cgi-bin/abnetopac/O9175/ID283f0afd"
	content = strings.TrimSuffix(content, MetaSuffix)

	// Por último ponemos el dominio de la página delante y devolvemos el valor
	// "http://www.madrid.org/biblio_publicas/cgi-bin/abnetopac/O9175/ID283f0afd"
	return RootUrl + content
}

// Descarga 'urlPagina', convierte el texto de codificación ISO-8859-1 a UTF-8
// para poder trabajar con los acentos y caracteres especiales de manera
// adecuada y finalmente analiza el código HTML y lo devuelve junto con un
// error, si lo hay.
func DescargarPagina(urlPagina string) (*goquery.Document, error) {
	// Usamos el cliente HTTP para hacer una petición GET y comprobamos errores
	respuesta, err := client.Get(urlPagina)
	if err != nil {
		return nil, err
	}

	// Esto es magia de Go, básicamente se asegura que una vez haya acabado la
	// función se llame al Body.Close de respuesta.
	defer respuesta.Body.Close()

	// El portal del OPAC usa codificación ISO-8859-1 pero nosotros queremos
	// trabajar con UTF-8, esto convierte de una codificación a otra.
	lector := charmap.ISO8859_1.NewDecoder().Reader(respuesta.Body)

	// Por último usamos la librería 'goquery' para analizar el código y poder
	// hacer búsquedas rápidamente. Como queremos devolver el documento para
	// usarlo en otros sitios simplemente hacemos return.
	return goquery.NewDocumentFromReader(lector)
}

func EnviarFormularioNuevasAdquisiciones(sesion, codigoBiblioteca string) (string, *goquery.Document) {
	// http://www.madrid.org/biblio_publicas/cgi-bin/abnetopac/O9247/ID1f0c1ec3/NT2
	urlFormulario := sesion + "/NT2"

	camposFormulario := url.Values{
		"ACC":       {"131"},            // Campo oculto
		"xindex":    {""},               // Campo oculto
		"xindty":    {""},               // Campo oculto
		"xsqf99":    {"1"},              // Campo oculto
		"xsoi99":    {"5"},              // Campo oculto
		"groupb":    {"B"},              // Campo oculto
		"groups":    {"S"},              // Campo oculto
		"groupg":    {"BG"},             // Campo oculto
		"subcat":    {codigoBiblioteca}, // Biblioteca: Cardenal cisneros
		"xsdate":    {"30"},             // Último mes
		"select03":  {"02"},             // Formato: Libros
		"xssort":    {"14216456"},       // Orden: Fecha de publicación
		"xsnlis":    {"10"},             // Libros por página
		"butt_send": {"Buscar"},         // Opcional? Puede que no haga falta enviarlo
	}

	respuesta, err := http.PostForm(urlFormulario, camposFormulario)
	AbortarSi(err)

	// fmt.Println("StatusCode:", respuesta.StatusCode)

	documento, err := goquery.NewDocumentFromResponse(respuesta)
	AbortarSi(err)

	return urlFormulario, documento
}

func ExtraerTotalLibros(doc *goquery.Document) int {
	valor := doc.Find("#results div.reglistr strong").Eq(1).Text()

	// 'strconv.Atoi' convierte texto en números
	total, err := strconv.Atoi(valor)
	if err != nil {
		return 0
	}

	return total
}

func ExtraerURLPatron(doc *goquery.Document) string {
	value, _ := doc.Find("#results div.dvdoc:nth-child(1) a").Eq(2).Attr("href")

	return value
}

func DescargarYGuardarLibro(
	db *hood.Hood,
	codigoBiblioteca string,
	urlLibro string,
	numeroBien *uint32,
	numeroMal *uint32,
	barraProgreso *uiprogress.Bar,
) {
	// Nos aseguramos de que pase lo que pase, cuando esta función termine
	// incrementamos la barra de progreso.
	defer barraProgreso.Incr()

	// Descargamos la página del libro.
	doc, err := DescargarPagina(urlLibro)
	if err != nil {
		// Si hay algún error, incrementamos el contador de libros fallidos y la
		// función termina.
		atomic.AddUint32(numeroMal, 1)
		return
	}

	info := ExtraerInformacionDeDocumento(doc)

	// Intentamos guardar el libro con todos los datos que hemos obtenido en la
	// base de datos.
	err = GuardarLibroEnBaseDeDatos(db, codigoBiblioteca, urlLibro, info)
	if err != nil {
		// De nuevo, puede haber algún error al guardar en la base de datos así
		// que incrementamos el contador de libros fallidos y la función
		// termina.
		atomic.AddUint32(numeroMal, 1)
		return
	}

	// Si no hay fallos llegaremos a este punto así que incrementamos el
	// contador de libros que se han bajado y guardado bien.
	atomic.AddUint32(numeroBien, 1)
}

func ExtraerInformacionDeDocumento(doc *goquery.Document) map[string]string {
	const Selector = "#abndocu > div.listadoc > div > div"

	// Creamos un diccionario vacío
	info := make(map[string]string)

	var listaValores []string
	doc.Find(Selector).Each(func(idx int, sel *goquery.Selection) {
		valor := strings.TrimSpace(sel.Text())
		// Guarda valor en 'listaValores'
		listaValores = append(listaValores, valor)
	})

	// i va desde 0 hasta el número de elementos que haya en 'listaValores'
	for i := 0; i < len(listaValores); i += 2 {
		campo := listaValores[i]   // i   = 0, 2, 4, ...
		valor := listaValores[i+1] // i+1 = 1, 3, 5, ...

		campo = strings.TrimSuffix(campo, ":")

		// Guardamos el par 'campo/valor' en el diccionario
		info[campo] = valor
	}

	return info
}

func GuardarLibroEnBaseDeDatos(
	conexionBaseDatos *hood.Hood,
	codigoBiblioteca string,
	urlLibro string,
	info map[string]string,
) error {
	const (
		CampoAutor  = "Autor/Autora"
		CampoTitulo = "Título"
	)

	book := &modelos.Libro{
		Autor:            info[CampoAutor],
		Titulo:           info[CampoTitulo],
		CodigoBiblioteca: codigoBiblioteca,
		Url:              urlLibro,
	}
	id, err := conexionBaseDatos.Save(book)
	if err != nil {
		return fmt.Errorf("error al guardar libro %q - %q: %s", book.Autor, book.Titulo, err)
	}

	for campo, value := range info {
		if campo == CampoAutor || campo == CampoTitulo {
			continue
		}

		metadata := &modelos.Metadatos{
			IdLibro: int64(id),
			Clave:   campo,
			Valor:   value,
		}

		// Los errores al guardar metadatos los vamos a ignorar porque lo
		// importante ya lo tenemos: Autor y Titulo.
		_, err = conexionBaseDatos.Save(metadata)
		_ = err // Descartamos errores! Qué puede salir mal?
	}

	return nil
}

func CrearTablasBaseDeDatos() {
	fmt.Print("Creating database tables... ")

	db := crawler.ConectarConBaseDeDatos()

	tablaBiblioteca := &modelos.Biblioteca{}
	tablaLibro := &modelos.Libro{}
	tablaMetadatos := &modelos.Metadatos{}

	tx := db.Begin()

	AbortarSi(tx.CreateTableIfNotExists(tablaBiblioteca))
	AbortarSi(tx.CreateTableIfNotExists(tablaLibro))
	AbortarSi(tx.CreateTableIfNotExists(tablaMetadatos))

	// Creación de índices para que las búsquedas sean más rápidas
	var err error
	_, err = tx.Exec("CREATE INDEX libro_autor_idx ON libro USING gin (to_tsvector('spanish', autor))")
	AbortarSi(err)
	_, err = tx.Exec("CREATE INDEX libro_titulo_idx ON libro USING gin (to_tsvector('spanish', titulo))")
	AbortarSi(err)
	_, err = tx.Exec("CREATE INDEX metadatos_valor_idx ON metadatos USING gin (to_tsvector('spanish', valor))")
	AbortarSi(err)
	_, err = tx.Exec("CREATE INDEX metadatos_clave_idx ON metadatos USING btree (valor)")
	AbortarSi(err)

	AbortarSi(tx.Commit())

	fmt.Println(color.GreenString("ok"))
}

func BorrarTablasBaseDeDatos() {
	fmt.Print("Removing database tables... ")

	db := crawler.ConectarConBaseDeDatos()

	tablaBiblioteca := &modelos.Biblioteca{}
	tablaLibro := &modelos.Libro{}
	tablaMetadatos := &modelos.Metadatos{}

	tx := db.Begin()
	AbortarSi(tx.DropTableIfExists(tablaBiblioteca))
	AbortarSi(tx.DropTableIfExists(tablaLibro))
	AbortarSi(tx.DropTableIfExists(tablaMetadatos))
	AbortarSi(tx.Commit())

	fmt.Println(color.GreenString("ok"))
}
