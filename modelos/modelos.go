package modelos

import "github.com/eaigner/hood"

type Biblioteca struct {
	Id           hood.Id
	Nombre       string
	Codigo       string
	Lugar        string
	CodigoPostal string
}

type Libro struct {
	Id               hood.Id
	CodigoBiblioteca string
	Url              string
	Autor            string
	Titulo           string
}

type Metadatos struct {
	Id      hood.Id
	IdLibro int64
	Clave   string
	Valor   string
}
